---
layout: page
title: About
permalink: /about/
---

This is the website of our project for EURECOM OS class. This project is done by Abbe KHALIFA, Julie BONNASSIEUX, Pauline TRONCY and Jordan MEALLIER. We have to build and program a robot to allow him to pick a ball in an arena and to shoot the balls in a basketball hoop.